import React from 'react';
import styles from './Card.scss';
import PropTypes from 'prop-types';

class Card extends React.Component {
  static propTypes = {
    text: PropTypes.string,
    action: PropTypes.func,
    title: PropTypes.string,
  }

  static defaultProps = {
    text: 'Add new item',
  }

  state = {
    value: '',
    visibleButtons: false,
  }

  handleChange(event){
    // console.log(event);
    const {value} = event.target;
    this.setState({
      value,
      visibleButtons: value.length > 0,
    });
  }

  handleOK(){
    if(this.state.value != ''){
      this.props.action(this.state.value);
      this.setState({
        value: '',
        visibleButtons: false,
      });
    }
  }

  handleCancel(){
    this.setState({
      value: '',
      visibleButtons: false,
    });
  }

  render() {
    const {title} = this.props;
    return (
      <div className={styles.component}>
        <h1 className={styles.title}>{title}</h1>
      </div>
    );
  }
}

export default Card;
